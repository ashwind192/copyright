<?php

namespace Artworksit\Copyright;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class CopyrightServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->registerDirectives();
    }

    /**
     * Register all directives.
     *
     * @return void
     */
    public function registerDirectives()
    {
        Blade::directive('copyright', function ($expression) {
            $curYear = date('Y');
            if ($expression != "''" && $curYear != $expression && $expression != null){
                return "<?php echo '© ' . {$expression} .' - '. {$curYear} .' Copyright'; ?>";
            }
            else{
                return "<?php echo '© ' . {$curYear} .' Copyright'; ?>";
            }

        });
    }
}